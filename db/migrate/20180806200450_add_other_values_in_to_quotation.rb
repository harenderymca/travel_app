class AddOtherValuesInToQuotation < ActiveRecord::Migration[5.2]
  def change
  	add_column :quotations, :trip_type, :string
  	add_column :quotations, :origin, :string
  	add_column :quotations, :destination, :string
  	add_column :quotations, :starttime, :datetime
  	add_column :quotations, :returntime, :datetime
    add_column :quotations, :name, :string
    add_column :quotations, :phone, :integer
    add_column :quotations, :email, :string
    add_column :quotations, :Adult, :integer
    add_column :quotations, :infants, :integer
    add_column :quotations, :children, :integer
    add_column :quotations, :airlinepreference1, :string
    add_column :quotations, :airlinepreference2, :string
    add_column :quotations, :classofeconomy, :string
    add_column :quotations, :mealrequest, :string
    add_column :quotations, :currency, :string
  end
end
