class CreateQuotations < ActiveRecord::Migration[5.2]
  def change
    create_table :quotations do |t|
      t.datetime :start_date
      t.string :location
      t.datetime :return_date
      t.integer :persons
      t.string :visit_purpose

      t.timestamps
    end
  end
end
