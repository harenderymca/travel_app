Rails.application.routes.draw do
  resources :quotations, only: %i[new create show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "home#index"
  post "/contact", to: "home#save_query"
  post "/discount", to: "home#save_discount"
  get  "/indian_tour", to: "home#indian_tour"
  get  "/quotations", to:"quotations#new"
  get  "useful_information", to:"home#useful_information"
  get  "baggage_info", to:"home#baggage_info"
  get  "airline_info", to:"home#airline_info"
  get  "airport_info", to:"home#airport_info"  

end
