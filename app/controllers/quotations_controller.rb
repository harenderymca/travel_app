class QuotationsController < ApplicationController
  before_action :set_quotation, only: [:show, :edit, :update, :destroy]

  # GET /quotations
  # GET /quotations.json
  def index
    @quotations = Quotation.all
  end

  # GET /quotations/1
  # GET /quotations/1.json
  def show
  end

  # GET /quotations/new
  def new
    @quotation = Quotation.new
  end

  # GET /quotations/1/edit
  def edit
  end

  # POST /quotations
  # POST /quotations.json
  def create
    @quotation = Quotation.new(quotation_params)
    begin
      @quotation.trip_type = params[:tripType]
      @quotation.Adult = params[:Adult].to_i
      @quotation.infants = params[:infants].to_i
      @quotation.children = params[:children].to_i
      @quotation.airlinepreference1 = params[:airlinepreference1]
      @quotation.airlinepreference2 = params[:airlinepreference2]
      @quotation.classofeconomy = params[:classofeconomy]
      @quotation.mealrequest = params[:mealrequest]
      @quotation.currency = params[:currency]
      @quotation.starttime = params[:starttime]
      @quotation.returntime = params[:returntime]
    rescue
    end  
    respond_to do |format|
      if @quotation.save
        Mailer.send_qatation_email_admin(@quotation).deliver
        format.html { redirect_to @quotation, notice: 'Your enquiry was successfully created.' }
        format.json { render :show, status: :created, location: @quotation }
      else
        format.html { render :new }
        format.json { render json: @quotation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quotations/1
  # PATCH/PUT /quotations/1.json
  def update
    respond_to do |format|
      if @quotation.update(quotation_params)
        format.html { redirect_to @quotation, notice: 'Quotation was successfully updated.' }
        format.json { render :show, status: :ok, location: @quotation }
      else
        format.html { render :edit }
        format.json { render json: @quotation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quotations/1
  # DELETE /quotations/1.json
  def destroy
    @quotation.destroy
    respond_to do |format|
      format.html { redirect_to quotations_url, notice: 'Quotation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quotation
      @quotation = Quotation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quotation_params
      params.require(:quotation).permit(:start_date, :location, :return_date, :persons, :visit_purpose,:origin,:name,:phone,:email,:destination)
    end
end
