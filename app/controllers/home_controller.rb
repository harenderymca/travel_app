class HomeController < ApplicationController

	def index
	end

	def save_query
		params.permit!
		@contact = Contact.new
		@contact.name = params[:name]
		@contact.phone = params[:phone]
		@contact.subject = params[:subject]
		@contact.message = params[:message]
		
		if @contact.save
			Mailer.send_email_admin(@contact).deliver
			respond_to do |format|
        		format.html
    		end
		else
			@contact
		  	render :index
		end
	end

	def save_discount
		params.permit!
		discount = Discount.new
		discount.name = params[:name]
		discount.email = params[:email]
		if discount.save!
			render :index
		end
	end

	def indian_tour
		respond_to do |format|
        format.html
    end
  end
  
  def useful_information
  	respond_to do |format|
        format.html
    end
	end
	def baggage_info
  	respond_to do |format|
        format.html
    end
	end	
  
  def airline_info
  	respond_to do |format|
        format.html
    end
	end

	def airport_info
  	respond_to do |format|
        format.html
    end
	end		
end
