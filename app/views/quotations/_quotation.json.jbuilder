json.extract! quotation, :id, :start_date, :location, :return_date, :persons, :visit_purpose, :created_at, :updated_at
json.url quotation_url(quotation, format: :json)
