class ApplicationMailer < ActionMailer::Base
  include SendGrid
  default from: 'support@fairdealtravels.ca'
  layout 'mailer'

  def send_email_admin(contact)
  	@user_name = contact.name
  	@user_phone = contact.phone
  	subject = "Email from #{contact.name} regarding travel plan"
  	@message = contact.message
  	reciever = "sukhmander.atwal@gmail.com"
    #body = message || ''
    mail(to: reciever, subject: subject)
  end

end
