class Contact < ApplicationRecord
	validates_presence_of :name, :message => "Please provide your name. "
	#validates_presence_of :email, :message => "Please provide your email."
	validates_presence_of :phone, :message => "Please provide your phone no."
	validates_presence_of :message, :message => "Please provide your message."
	validates_presence_of :subject, :message => "Please provide your subjects."

	# validates :name, presence: true
	# validates :email, presence: true
	# validates :phone, presence: true
	# validates :message, presence: true
	# validates :subject, presence: true
end
